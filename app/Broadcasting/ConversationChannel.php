<?php


namespace App\Broadcasting;

use App\Models\User;

class ConversationChannel
{
    public function join(User $user, $chat)
    {
        return $chat->users->contains($user);
    }
}
