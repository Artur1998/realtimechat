<?php

namespace App\Http\Controllers;

use App\Events\Typing;
use App\Events\NewMessage;
use App\Models\Message;
use App\Models\User;
use App\Notifications\NewPostNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ContactsController extends Controller
{
    public function get()
    {
        $contacts = User::where('id', '!=', auth()->id())->get();

        return response()->json($contacts);
    }

    public function getMessagesFor($id)
    {
        $messages = Message::where('from', $id)->orWhere('to', $id)->get();
        return response()->json($messages);
    }

    public function send(Request $request)
    {

        $message = Message::create([
            'from' => auth()->user()->id,
            'to' => $request->contact_id,
            'text' => $request->text
        ]);
        event(new NewMessage($message));


        $user = User::find($request->contact_id);

        Notification::send($user, new NewPostNotification($message, auth()->user()));


        return response()->json($message);

    }
    public function isTyping(Request $request, $to)
    {
        event(new Typing($to, $request->is_typing));
    }

}
