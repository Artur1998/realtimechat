<?php

use App\Http\Controllers\Chat\ChatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('chat');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/chat', [ChatController::class, 'index']);


Route::get('/contacts', [\App\Http\Controllers\ContactsController::class, 'get']);
Route::get('/conversation/{id}', [\App\Http\Controllers\ContactsController::class, 'getMessagesFor']);
Route::post('/conversation/send', [\App\Http\Controllers\ContactsController::class, 'send']);
Route::post('/conversation/isTyping/{to}', [\App\Http\Controllers\ContactsController::class, 'isTyping']);
